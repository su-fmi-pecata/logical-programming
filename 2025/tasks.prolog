:- use_module(library(clpfd)).
% Maybe you will find other valuable predicates, but I think that these are the top "must know".

% Comments start with %
% Statements end with a .
% Variables start with an Upper Case
% Atoms start with a lower case
% Anonymos variables start with a _ 
% \+ is the negation as failure operator. It's used to express that a particular goal cannot be proven. Best to use with brackets. Can be swapped with not()
% !. stops after returning the first answer
% It's best if we have only one stopper of the reccursion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Definition of list
isList([]).
isList([_F|LB]) :- isList(LB).

% append
appendInList([], L, L).
appendInList([H|F], S, [H|R]) :- appendInList(F, S, R).

% Is X a member of the list L
isMemberRecc(X, [X|_]).
isMemberRecc(X, [Y|L]) :- X \= Y, isMemberRecc(X, L).

isMember(X, L) :- appendInList(_, [X|_], L).

% Insert X in the list L arbitrary
insertInList(X, L, R) :- appendInList(A, B, L), appendInList(A, [X|B], R).

% Remove X in the list L (Only from one position, if X is contained more than once in L)
removeFromListMultipleChoices(X, L, R) :- appendInList(A, [X|B], L), appendInList(A, B, R). % gives multiple outputs

removeFromList(_, [], []).
removeFromList(X, [X|Xl], Xl).
removeFromList(X, [Y|Xl], [Y|R]) :- X \= Y, removeFromList(X, Xl, R).


% Permute the list L
permuteList([], []).
permuteList([H|L], R) :- permuteList(L, Q), insertInList(H, Q, R).

% Check if sorted
isSortedReccAsc([]).
isSortedReccAsc([_]).
isSortedReccAsc([A,B|L]) :- A=<B, isSortedReccAsc([B|L]).

isSortedReccDesc([]).
isSortedReccDesc([_]).
isSortedReccDesc([A,B|L]) :- A>=B, isSortedReccDesc([B|L]).


isSortedAppAsc(L) :- \+ (appendInList(_, [A,B|_], L), A > B).
isSortedAppDesc(L) :- \+ (appendInList(_, [A,B|_], L), A < B).

% Some sorting notes:
% The Recc sorting gives us more results, than expected.
% Let's say we have the list [1,2,3,3,4,4] which we want to sort.
% With the recc, we will get 4 results, as of the App we get only 2.
% Both of these require deduplication of the result.


% Deduplicate elements in list
deduplicateList(List, Result) :- deduplicateListHelper(List, [], Result).

deduplicateListHelper([], R, R).
deduplicateListHelper([X|Left], Seen, Result) :- isMember(X, Seen), deduplicateListHelper(Left, Seen, Result).
deduplicateListHelper([X|Left], Seen, Result) :- \+ isMember(X, Seen), deduplicateListHelper(Left, [X|Seen], Result).


% Simple sort
sortListAsc(L, R) :- permuteList(L, R), isSortedAppAsc(R), !.
sortListDesc(L, R) :- permuteList(L, R), isSortedAppDesc(R), !.

% Length of list
lengthOfList([], 0).
lengthOfList([_|L], R) :- lengthOfList(L, P), R is P + 1.

% Minimal element of list
minInList(X, L) :- permuteList(L, [X|R]), isSortedReccAsc([X|R]).

minInListMember(X, L) :- isMember(X, L), \+ (isMember(Y, L), Y < X).

% Maximal element of list
maxInList(X, L) :- permuteList(L, [X|R]), isSortedReccDesc([X|R]).

maxInLIstMember(X, L) :- isMember(X, L), \+ (isMember(Y, L), Y > X).

% Prefix of list
prefixOfList(P, L) :- appendInList(P, _, L).

% Suffix of list
suffixOfList(S, L) :- appendInList(_, S, L).

% Sublist of list(a slice/chunk of the list)
subListOfList(_, []).
subListOfList(L, R) :- prefixOfList(P, L), suffixOfList(R, P), R \= [].

% Subset of L - any combination of the elements
subsetOfList([], []).
subsetOfList([_|L], R) :- subsetOfList(L, R).
subsetOfList([H|L], [H|R]) :- subsetOfList(L, R).

% Split L in two subsets
splitList([], [], []).
splitList([A|L], [A|R1], R2) :- splitList(L, R1, R2).
splitList([A|L], R1, [A|R2]) :- splitList(L, R1, R2).

% Get element at position N

% The proposed solution does not work, as it looks from right to left
% getElementAtPositionLength(Pos, [El|List], El) :- lengthOfList([El|List], Pos).
% getElementAtPositionLength(Pos, [A|List], El) :- lengthOfList([A|List], Len), Len > Pos, getElementAtPositionLength(Pos, List, El).

getElementAtPositionRecc(1, [El|_], El).
getElementAtPositionRecc(Pos, [_|L], El) :- getElementAtPositionRecc(Prev, L, El), Pos is Prev + 1.

% Count occurrences of Y in L
countOccurences(_, [], 0).
countOccurences(El, [El|List], Res) :- countOccurences(El, List, Res1), Res is Res1 + 1.
countOccurences(El, [A|List], Res) :- countOccurences(El, List, Res), El \= A.

% Reverse of lists
reverseList([], []).
reverseList([Head|List], Res) :- reverseList(List, Rev), appendInList(Rev, [Head], Res).

% Divide list in smaller lists
/*
?- divide([1,2,3],L).
L = [[1], [2], [3]] ; -- will not be returned by splitList
L = [[1], [2, 3]] ;
L = [[1, 2], [3]] ;
L = [[1, 2, 3]] ;
false.
*/
divideListApp([], []).
divideListApp(L, [H|R]) :- appendInList(H, P, L), H \= [], divideListApp(P, R).

% Flattern - opposite of divide
makeFlat([], []).
makeFlat([X|L], [X|R]) :- \+ isList(X), makeFlat(L, R).
makeFlat([X|L], R) :- isList(X), makeFlat(X, Xflat), makeFlat(L, Lflat), appendInList(Xflat, Lflat, R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% in_union
unionList(L1, [], L1):- !.
unionList([], L2, L2):- !.
unionList([A|L1], [B|L2], [A,B|R]) :- unionList(L1, L2, R).

inUnion(L1, L2, X) :- member(X, L1); member(X, L2).

% in_intersection
intersectionList([], _, []).
intersectionList([X|L1], L2, [X|R]) :- isMember(X, L2), intersectionList(L1, L2, R).
intersectionList([X|L1], L2, R) :- \+ isMember(X, L2), intersectionList(L1, L2, R).

inIntersection(L1, L2, X) :- isMember(X, L1), isMember(X, L2).

% in_difference
differenceList(L1, L2, R) :- unionList(L1, L2, Union), intersectionList(L1, L2, Intersection), differenceListHelper(Union, Intersection, R).
differenceListHelper([], _, []).
differenceListHelper([X|Union], Intersection, [X|R]) :- (\+ isMember(X, Intersection)), differenceListHelper(Union, Intersection, R).
differenceListHelper([X|Union], Intersection, R) :- isMember(X, Intersection), differenceListHelper(Union, Intersection, R).

inDifference(L1, L2, X) :- isMember(X, L1), \+ isMember(X, L2).
inDifference(L1, L2, X) :- isMember(X, L2), \+ isMember(X, L1).
inDifferenceInter(L1, L2, X) :- inUnion(L1, L2, X), \+ inIntersection(L1, L2, X).

% is_subset_of
isSubsetOf([], _).
isSubsetOf([H|L1], L2) :- isMember(H, L2), removeFromList(H, L2, R), isSubsetOf(L1, R).

isSubsetOfWrong([], _).
isSubsetOfWrong([H|L1], L2) :- isMember(H, L2), isSubsetOfWrong(L1, L2). % wrong because isSubsetOfWrong([1], [1,1]) is true.

% are_equal
areEqual(L1, L2) :- isSubsetOf(L1, L2), isSubsetOf(L2, L1). % works only if isSubsetOf([1], [1,1]) is false.

% Erase duplicates in list L
eraseDuplicatesWrong([], []).
eraseDuplicatesWrong([X|L], [X|R]) :- not(isMember(X, R)), eraseDuplicatesWrong(L, R).
eraseDuplicatesWrong([X|L], R) :- isMember(X, R), eraseDuplicatesWrong(L, R).
% eraseDuplicatesWrong is ot working as it's trying to create too much calls. This is due to the fact, that it needs to check 
% the data in R, which is not populated yet. To prevent this, we need an additional parameter, which to contain only the deduplicated values

% eraseDuplicates \2
eraseDuplicates(L, R) :- eraseDuplicates(L, [], R).

% eraseDuplicates \3
eraseDuplicates([], Set, Set).
eraseDuplicates([X|L], Set, R) :- isMember(X, Set), eraseDuplicates(L, Set, R).
eraseDuplicates([X|L], Set, R) :- not(isMember(X, Set)), eraseDuplicates(L, [X|Set], R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Given the set of ribs E, get V the set of vertices
% extractVertices \2
extractVertices(Ribs, Vert) :- extractVerticesDuplicated(Ribs, DupVert), eraseDuplicates(DupVert, Vert).

% extractVerticesDuplicated \2
extractVerticesDuplicated([], []).
extractVerticesDuplicated([[V1,V2]|Ribs], [V1,V2|Vert]) :- extractVerticesDuplicated(Ribs, Vert).

% Find an path in the graph
% findPath \4
findPath(X, X, _, []).
findPath(X, Y, Ribs, [X,Y]) :- isMember([X,Y], Ribs).
findPath(X, Y, Ribs, [X|Path]) :- not(isMember([X,Y], Ribs)), isMember([X,P], Ribs), removeFromList([X,P], Ribs, OtherRibs), findPath(P, Y, OtherRibs, Path).

% Find an acyclic path in the graph
% findPathAcyclic \4
findPathAcyclic(X, Y, Ribs, Path) :- findPathAcyclic(X, Y, Ribs, [X], Path).

% findPathAcyclic \4
findPathAcyclic(X, X, _, _, []).
findPathAcyclic(X, Y, Ribs, _, [X,Y]) :- isMember([X,Y], Ribs).
findPathAcyclic(X, Y, Ribs, Visited, [X|Path]) :- 
    not(isMember([X, Y], Ribs)), 
    isMember([X,P], Ribs), 
    not(isMember(P, Visited)), 
    removeFromList([X,P], Ribs, OtherRibs), 
    findPathAcyclic(P, Y, OtherRibs, [P|Visited], Path).

% When is there a cycle in the graph?
hasCycle(Ribs) :- extractVertices(Ribs, Vert), isMember(V, Vert), findPath(V, V, Ribs, Paths), lengthOfList(Paths, Len), Len > 0.

% When is the graph connected?
% isConnected([Ribs]) :-  extractVertices(Ribs, [A|Vert]), isMember(B,Vert), findPath 

/* Generate all trees defined
 *   [1] is a tree
 *   if A is a tree and B is a list of trees then [A|B] is a tree
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate numbers in interval [A, B].

% Generate natural numbers

% Generate integers

% Generate a list of numbers in interval [A, B]

% Generate a pair of naturals

% Generate K numbers with sum S

% Generate all finite lists of natural numbers


% Find the GCD of two numbers
