# Logical Programming


### Prolog functions
| syntax | function | args # | result |
| ------ | -------- | ------ | ------ |
| `\+ /1` | `\+` | 1 | negation |
| `not /1` | `not` | 1 | negation |
| `, /2` | `,` | 2 | AND |
| `; /2` | `;` | 2 | OR |
