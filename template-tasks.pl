:- use_module(library(clpfd)).
% Maybe you will find other valuable predicates, but I think that these are the top "must know".

% Comments start with %
% Statements end with a .
% Variables start with an Upper Case
% Atoms start with a lower case
% Anonymos variables start with a _ 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Definition of list

% append

% Is X a member of the list L

% Insert X in the list L arbitrary

% Remove X in the list L (Only from one position, if X is contained more than once in L)

% Permute the list L

% Check if sorted

% Simple sort

% Deduplicate elements in list

% Length of list

% Minimal element of list

% Maximal element of list

% Prefix of list

% Suffix of list

% Sublist of list(a slice/chunk of the list)

% Subset of L

% Split L in two subsets

% Get element at position N

% Count occurrences of Y in L

% Reverse of lists

% Divide list in smaller lists
/*
?- divide([1,2,3],L).
L = [[1], [2], [3]] ;
L = [[1], [2, 3]] ;
L = [[1, 2], [3]] ;
L = [[1, 2, 3]] ;
false.
*/

% Flattern - opposite of divide

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% in_union

% in_intersection

% in_difference

% is_subset_of

% are_equal

% Erase duplicates in list L

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Given the set of ribs E, get V the set of vertices

% Find an acyclic path in the graph

% When is there a cycle in the graph?

% When is the graph connected?

/* Generate all trees defined
 *   [1] is a tree
 *   if A is a tree and B is a list of trees then [A|B] is a tree
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate numbers in interval [A, B].

% Generate natural numbers

% Generate integers

% Generate a list of numbers in interval [A, B]

% Generate a pair of naturals

% Generate K numbers with sum S

% Generate all finite lists of natural numbers


% Find the GCD of two numbers
