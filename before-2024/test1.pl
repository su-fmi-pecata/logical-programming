% Maybe you will find other valuable predicates, but I think that these are the top "must know".

% Comments start with %
% Statements end with a .
% Variables start with an Upper Case
% Atoms start with a lower case
% Anonymos variables start with a _ 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Definition of list
isList1([]).
isList1([_|T]) :- isList1(T).

% Append
append1([], L, L).
append1([H|T], L, [H|R]) :- append1(T, L, R).

% Is X a member of the list L
member1(X, T) :- append1(_, [X|_], T).

% Insert X in the list L arbitrary
insert1(X, L, R) :- append1(A, B, L), append1(A, [X|B], R).

% Remove X in the list L (Only from one position, if X is contained more than once in L)
remove1(X, L, R) :- append1(A, [X|B], L), append1(A, B, R).

% Permute the list L
permute1([], []).
permute1([H|T], L) :- permute1(T, Q), insert1(H, Q, L).

% Check if sorted
isSorted1(L) :- \+ (append1(_, [A,B|_], L), A > B).

% Simple sort
simpleSort1(X, R) :- permute1(X, R), isSorted1(R).

% Length of list
len1([], 0).
len1([_|T], N) :- len1(T, M), N is M + 1.

% Minimal element of list
min1(X, L) :- member1(X, L), \+ (member1(Y, L), Y \= X, Y < X).

% Maximal element of list
max1(X, L) :- member1(X, L), \+ (member1(Y, L), Y \= X, Y > X).

% Prefix of list
prefix1(L, R) :- append(R, _, L).

% Suffix of list
suffix1(L, R) :- append(_, R, L).

% Sublist of list(a slice/chunk of the list)
sublist1(L, R) :- prefix1(L, P), suffix1(P, R).

% Subset of L
subset1([], []).
subset1([_|L], R) :- subset1(L, R).
subset1([H|L], [H|R]) :- subset1(L, R).

% Split L in two subsets
split2([], [], []).
split2([H|T], [H|A], B) :- split2(T, A, B).
split2([H|T], A, [H|B]) :- split2(T, A, B).

% Get element at position N
nthElem1(1, [X|_], X).
nthElem1(N, [_|T], X) :- nthElem1(M, T, X), N is M + 1.

% Count occurrences of Y in L
count1(_, [], 0).
count1(X, [X|T], N) :- count1(X, T, M), N is M + 1.
count1(X, [A|T], N) :- count1(X, T, N), X \= A.

% Reverse of lists
rev1([X], [X]).
rev1([H|T], R) :- rev1(T, B), append1(B, [H], R).

% Divide list in smaller lists
divide1([], []).
divide1(L, [H|R]) :- append1(H, T, L), H \= [], divide1(T, R).
/*
?- divide([1,2,3],L).
L = [[1], [2], [3]] ;
L = [[1], [2, 3]] ;
L = [[1, 2], [3]] ;
L = [[1, 2, 3]] ;
false.
*/

% Flattern - opposite of divide
flatten1([], []). % flats single level
flatten1([H|T], [H|R]) :-  not(isList1(H)), flatten1(T, R).
flatten1([H|T], R) :- isList1(H), flatten1(T, Q), append1(H, Q, R).

flatten2([], []). % flats all levels
flatten2(X, [X]) :- not(isList1(X)).
flatten2([H|T], R) :- flatten2(H, FH), flatten2(T, FT), append1(FH, FT, R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% in_union
inUnion1(A, B, X) :- member1(X, A); member1(X, B).

% in_intersection
inIntersection1(A, B, X) :- member1(X, A), member1(X, B).

% in_difference
inDifference1(A, B, X) :- inUnion1(A, B, X), not(inIntersection1(A, B, X)).

% is_subset_of
isSubset1([], _).
isSubset1([H|T], B) :- member1(H, B), isSubset1(T, B).

% are_equal
areEqual1([], []).
areEqual1([H|T], B) :- remove1(H, B, RB), areEqual1(T, RB).

% Erase duplicates in list L
eraseDuplicates1([], []).
eraseDuplicates1([H|T], R) :- eraseDuplicates1(T, R), member1(H, R).
eraseDuplicates1([H|T], [H|R]) :- eraseDuplicates1(T, R), not(member1(H, R)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Given the set of ribs E, get V the set of vertices
getVertices1(E, V) :- flatten2(E, FE), eraseDuplicates1(FE, V).

% Find an acyclic path in the graph

% When is there a cycle in the graph?

% When is the graph connected?

/* Generate all trees defined
 *   [1] is a tree
 *   if A is a tree and B is a list of trees then [A|B] is a tree
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate numbers in interval [A, B].

% Generate natural numbers

% Generate integers

% Generate a list of numbers in interval [A, B]

% Generate a pair of naturals

% Generate K numbers with sum S

% Generate all finite lists of natural numbers


% Find the GCD of two numbers
