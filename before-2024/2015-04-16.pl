:- use_module(library(clpfd)).

count_x_in_l(_, [], 0).
count_x_in_l(X, [X|T], G) :- G #= G1 + 1, count_x_in_l(X, T, G1).
count_x_in_l(X, [Y|T], G) :- X \= Y, count_x_in_l(X, T, G).

member_g_in_l(G, [G|_]).
member_g_in_l(G, [_|T]) :- member_g_in_l(G, T).

exist_more_occurences(X, L) :- member_g_in_l(Y, L), Y \= X, count_x_in_l(Y, L, R1), count_x_in_l(X, L, R2), R1 #> R2.

most_common_g_in_l(G, L) :- member_g_in_l(G, L), not(exist_more_occurences(G, L)), !.

subset2([], []).
subset2([H|T], [H|G]) :- subset2(T, G).
subset2([_|T], G) :- subset2(T, G).

most_common_count(L, C) :- most_common_g_in_l(X, L), count_x_in_l(X, L, C).

z(X, S) :- subset2(X, S), most_common_count(S, M), not(member_g_in_l(M, X)).


p(L, R) :- el_function(L, [], R), !.
el_function(L, T, R) :- z(L, X), not(member(X,T)), el_function(L, [X|T], R).
el_function(_, R, R) :- !.

