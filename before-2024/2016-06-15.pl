:- use_module(library(clpfd)).

subset([], []).
subset([H|T], [H|G]) :- subset(T, G).
subset([_|T], G) :- subset(T, G).

gcd(X, 0, X).
gcd(X, Y, G) :- X > Y, C #= X mod Y, gcd(Y, C, G).
gcd(X, Y, G) :- Y > X, gcd(Y, X, G).

base(L) :- not( (append(_, [X|LL], L), append(_, [Y|_], LL), gcd(X, Y, G), G \= 1)).

% gen(M, S, G) :- 

%gen([], S, S).
%gen(M, S, [T|L]) :- 
gen(M, L) :- subset(M, T), base(T), append(T, [], L).


% s(X, [], S, H) :- append()
% s([H|T])