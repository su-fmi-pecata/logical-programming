:- use_module(library(clpfd)).

append2([], L, L).
append2([H|T], L, [H|R]) :- append2(T, L, R).

member2([X|_], X).
member2([_|T], X) :- member2(T, X).

insert2(X, L, RL) :- append2(A, B, L), append2(A, [X|B], RL).

remove2(X, L, RL) :- append2(A, [X|B], L), append2(A, B, RL).

permutation2([], []).
permutation2([H|T], P) :- permutation2(T, Q), insert2(H, Q, P).

len2([], 0).
len2([_|T], G) :- G #= G1 + 1, len2(T, G1).

subset2([], []).
subset2([H|T], [H|R]) :- subset2(T, R).
subset2([_|T], R) :- subset2(T, R).

remove_duplicates2([], []).
remove_duplicates2([H|T], [H|R]) :- remove_duplicates2(T, R), not(member2(R, H)).
remove_duplicates2([H|T], R) :- remove_duplicates2(T, R), member2(R, H).


pred(X, S) :- subset2(X, S).

task(L, R) :- responses_to_list(L, [], R), !.
responses_to_list(L, T, R) :- pred(L, X), not(member(X, T)), responses_to_list(L, [X|T], R).
responses_to_list(_, R, R) :- !.
