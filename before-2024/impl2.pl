% Maybe you will find other valuable predicates, but I think that these are the top "must know".

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Definition of list
isList1([]).
isList1([_|T]) :- isList1(T).

% append
append1([], L2, L2).
append1([H|T], L2, [H|R]) :- append1(T, L2, R).

% Is X a member of the list L
member1(X, L) :- append1(_, [X|_], L).

% Insert X in the list L arbitrary
insert1(X, L, R) :- append1(A, B, L), append1(A, [X|B], R).

% Remove X in the list L (Only from one position, if X is contained more than once in L)
remove1(X, L, R) :- append1(A, [X|B], L), append1(A, B, R).

% Permute the list L
permute1([], []).
permute1([H|T], R) :- permute1(T, Q), insert1(H, Q, R).

% Check if sorted
isSorted1([]).
isSorted1([X|T]) :- isSorted1(T), not((member1(Y, T), X \= Y, X > Y)).

isSorted2(L) :- \+ (append1(_, [A,B|_], L), A > B).

% Simple sort
simpleSort1(L, R) :- permute1(L, R), isSorted2(R).

% Length of list
len1([], 0).
len1([_|T], N) :- len1(T, M), N is M + 1.

% Minimal element of list
min1(X, L) :- member1(X, L), not((member1(Y, L), X \= Y, Y < X)).

% Maximal element of list
max1(X, L) :- member1(X, L), not((member1(Y, L), X \= Y, Y > X)).

% Prefix of list
prefix1(L, R) :- append1(R, _, L).

% Suffix of list
suffix1(L, R) :- append1(_, R, L).

% Sublist of list(a slice/chunk of the list)
subList1(L, R) :- prefix1(L, T), suffix1(T, R).

% Subset of L
subset1([], []).
subset1([_|T], R) :- subset1(T, R).
subset1([H|T], R) :- subset1(T, Q), append1([H], Q, R).

% Split L in two subsets
split1([], _, _).
split1([H|T], [H|A], B) :- split1(T, A ,B).
split1([H|T], A, [H|B]) :- split1(T, A ,B).

% Get element at position N
getNth1([H|_], 0, H).
getNth1([_|T], N, H) :- getNth1(T, M, H), N is M + 1.

% Count occurrences of Y in L
count1(_, [], 0).
count1(X, [X|T], N) :- count1(X, T, M), N is M + 1.
count1(X, [Y|T], N) :- Y \= X, count1(X, T, N).

% Reverse of lists
rev1([], []).
rev1([H|T], R) :- rev1(T, A), append(A, [H], R).

% Divide list in smaller lists
/*
?- divide([1,2,3],L).
L = [[1], [2], [3]] ;
L = [[1], [2, 3]] ;
L = [[1, 2], [3]] ;
L = [[1, 2, 3]] ;
false.
*/

% Flattern - opposite of divide
flattern1([], []).
flattern1([H|T], R) :- isList1(H), flattern1(H, FH), flattern1(T, FT), append1(FH, FT, R).
flattern1([H|T], [H|R]) :- not(isList1(H)), flattern1(T, R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% in_union
inUnion1(X, A, B) :- member1(X, A); member1(X, B).

% in_intersection
inIntersection1(X, A, B) :- member1(X, A), member1(X, B).

% in_difference
inDifference1(X, A, B) :- inUnion1(X, A, B), not(inIntersection1(X, A, B)).

% is_subset_of
isSubsetOf1(A, B) :- not((member1(X, A), not(member(X, B)))).

% are_equal
areEqual1(A, B) :- isSubsetOf1(A, B), isSubsetOf1(B, A).

% Erase duplicates in list L
erase1([], []).
erase1([H|T], R) :- erase1(T, R), member1(H, R).
erase1([H|T], [H|R]) :- erase1(T, R), not(member1(H, R)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Graphs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Given the set of ribs E, get V the set of vertices
getVertices1(E, V) :- flattern1(E, T), erase1(T, V).

% Find an acyclic path in the graph

% When is there a cycle in the graph?

% When is the graph connected?

/* Generate all trees defined
 *   [1] is a tree
 *   if A is a tree and B is a list of trees then [A|B] is a tree
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Generate numbers in interval [A, B].
generateNumbers1(A, A, [A]).
generateNumbers1(A, B, [A|R]) :- A < B, N is A + 1, generateNumbers1(N, B, R).

% Generate natural numbers
integer(0, 0).
integer(X, Y) :- X > 0, (Y is -X; Y is X).

% Generate integers

% Generate a list of numbers in interval [A, B]

% Generate a pair of naturals

% Generate K numbers with sum S

% Generate all finite lists of natural numbers


% Find the GCD of two numbers
